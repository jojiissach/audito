from django.contrib.auth.models import User
from django.conf import settings
from django.core.mail import send_mail
from django.core.mail import EmailMessage


def send_email(to_address,subject,content,html_content,bcc_address=settings.DEFAULT_BCC_EMAIL,attachment=None,attachment2=None,attachment3=None):
    
    email = EmailMessage(
        subject,
        html_content,
        settings.DEFAULT_FROM_EMAIL,
        [to_address],
        [bcc_address],
        reply_to=[settings.DEFAULT_REPLY_TO_EMAIL],
    )
    email.content_subtype = 'html'
    email.send(fail_silently=True)


def create_notification(request,notification_type,instance=False):
    who = request.user
    subject = NotificationSubject.objects.get(code=notification_type)
    superusers = User.objects.filter(is_active=True,is_superuser=True)  

    if notification_type == "cash_on_delivary_order" :
        
        #create notification for super user 
        for superuser in superusers :    
            Notification(
                user = superuser,
                subject = subject,
                who = who,
                order = instance,
                customer = instance.customer
            ).save()

    if notification_type == "order_status_updated" :
        
        #create notification for super user 
        for superuser in superusers : 
            order_status =  instance.order_status.replace("_", " ")
            order_status.capitalize()   
            Notification(
                user = instance.customer.user,
                subject = subject,
                who = who,
                order = instance,
                customer = instance.customer,
                order_status = order_status
            ).save()

    if notification_type == "product_verified" :
        suppliers_pk = ProductStock.objects.filter(product=instance).values_list('supplier',flat=True)
        suppliers = Supplier.objects.filter(pk__in=suppliers_pk)

        #create notification for super user 
        for supplier in suppliers :
            if supplier.user :  
                Notification(
                    user = supplier.user,
                    subject = subject,
                    who = who,
                    product = instance,
                ).save()
            

    if notification_type == "product_unverified" :
        suppliers_pk = ProductStock.objects.filter(product=instance).values_list('supplier',flat=True)
        suppliers = Supplier.objects.filter(pk__in=suppliers_pk)

        #create notification for super user 
        for supplier in suppliers :  
            if supplier.user :
                Notification(
                    user = supplier.user,
                    subject = subject,
                    who = who,
                    product = instance,
                ).save()
            
            