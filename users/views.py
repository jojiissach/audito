from django.http.response import HttpResponse
import json
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect, HttpResponse
from django.views.decorators.http import require_GET
from django.contrib.auth.decorators import login_required
from django.shortcuts import resolve_url, render, get_object_or_404
from django.contrib.auth.tokens import default_token_generator
from django.template.response import TemplateResponse
from django.contrib.auth.models import User, Group
from users.forms import UserForm
from suppliers.models import Supplier
from users.models import Permission, Notification
from main.functions import generate_form_errors
from django.contrib.auth.forms import PasswordChangeForm
from registration.signals import user_registered
from django.utils import timezone
from datetime import date, time
import datetime
from main.decorators import permissions_required, ajax_required
from django.db.models import Q


@login_required
@permissions_required(['can_create_user'])
def create_user(request, pk): 
    supplier = get_object_or_404(Supplier.objects.filter(pk=pk))

    if request.method == "POST":
        form = UserForm(request.POST)
        response_data = {} 
        message = ""  
        data = []  
        emails = request.POST.get('email')
        email = str(emails)
        error = False
        if User.objects.filter(email=email).exists():
            error = True
            message += "This email already exists."
        if supplier.user:
            error = True
            message += "This supplier already has a user."
    
        if not error:
            if form.is_valid():    

                data = form.save(commit=False)
                data.email = email
                data.save()

                instance = data
                group = Group.objects.get(name="supplier")
                instance.groups.add(group)

                supplier.user=data
                supplier.save()
                
                #save permissions
                permissions = request.POST.getlist('permission')
                for perm in permissions:
                    p = Permission.objects.get(id=perm)
                    supplier.permissions.add(p)

                response_data = {
                    'status' : 'true',     
                    'title' : "User Created",       
                    'redirect' : 'true', 
                    'redirect_url' : reverse('suppliers:supplier', kwargs = {'pk' : pk}),
                    'message' : "User Created Successfully"
                }
    
            else:
                error = True
                response_data = {
                    'status' : 'false',
                    'stable' : 'true',
                    'title' : "Form validation error"
                }
                
                message = ''            
                message += generate_form_errors(form,formset=False)                
                response_data['message'] = message
    
        else:
            response_data = {
                'status' : 'false',     
                'title' : "Can't create this user",       
                'redirect' : 'true', 
                'redirect_url' : reverse('suppliers:suppliers'),
                'message' : message
            }
    
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = UserForm(initial={'email' : supplier.email})
        permissions = Permission.objects.all()
        products_permissions = permissions.filter(app="products")
        suppliers_permissions = permissions.filter(app="suppliers")
        users_permissions = permissions.filter(app="users")
        sales_permissions = permissions.filter(app="sales")
        customers_permissions = permissions.filter(app="customers")
        general_permissions = permissions.filter(app="main")
        
        context = {
            "form" : form,
            "title" : "Create User",
            "products_permissions" : products_permissions,
            "suppliers_permissions" : suppliers_permissions,
            "users_permissions" : users_permissions,
            "sales_permissions" : sales_permissions,
            "customers_permissions" : customers_permissions,
            "general_permissions" : general_permissions,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_grid_system": True,
            "is_need_animations": True,
            "is_need_datetime_picker": True,
            "is_need_checkbox" : True

            
        }
    return render(request, 'users/create_user.html', context)


@login_required
@permissions_required(['can_manage_user_permission'])
def change_permissions(request,pk):
    response_data = {}
    
    instance = get_object_or_404(Supplier.objects.filter(pk=pk))

    if request.method == "POST":

        #save permissions
        instance.permissions.clear()
        permissions = request.POST.getlist('permission')
        for perm in permissions:
            p = Permission.objects.get(id=perm)
            instance.permissions.add(p)

        response_data = {
            'status' : 'true',
            'title' : "Permission Changed",
            'redirect' : 'true',
            'redirect_url' : reverse('suppliers:supplier', kwargs = {'pk' : pk}),
            'message' : "Permissions Successfully Changed."
        }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:          
        permissions = Permission.objects.all()
        permissions = Permission.objects.all()
        products_permissions = permissions.filter(app="products")
        suppliers_permissions = permissions.filter(app="suppliers")
        users_permissions = permissions.filter(app="users")
        sales_permissions = permissions.filter(app="sales")
        customers_permissions = permissions.filter(app="customers")
        general_permissions = permissions.filter(app="main")
        
        active_permissions = instance.permissions.all()
        context = {
            "title" : "Change Permissions : " + instance.name,
            "active_permissions" : active_permissions,
            "instance" : instance,

            "products_permissions" : products_permissions,
            "suppliers_permissions" : suppliers_permissions,
            "users_permissions" : users_permissions,
            "sales_permissions" : sales_permissions,
            "customers_permissions" : customers_permissions,
            "general_permissions" : general_permissions,
            "redirect" : True,
            
            "is_need_popup_box": True,
            "is_need_grid_system": True,
            "is_need_animations": True,
            "is_need_datetime_picker": True,
            "is_need_checkbox" : True,
            
            "pk" : pk,
        }
        return render(request, 'users/change_permissions.html', context)


@ajax_required
@require_GET
def check_notification(request):
    user = request.user
    count = Notification.objects.filter(user=user,is_read=False).count()
    return HttpResponse(json.dumps(count), content_type='application/javascript')


@login_required
def notifications(request):
    title = "Notifications"
    
    instances = Notification.objects.filter(user=request.user,is_deleted=False)
    
    query = request.GET.get("search")
    if query:
        instances = instances.filter(Q(subject__name__icontains=query))
        title = "Notifications - %s" %query 

    context = {
        'title' : title,
        "instances" : instances,

        "is_need_popup_box": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_checkbox": True
    }
    return render(request,"users/notifications.html",context) 


@login_required
def delete_notification(request,pk):
    Notification.objects.filter(pk=pk,user=request.user).update(is_deleted=True,is_read=True)
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Notification Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('users:notifications')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@login_required
def delete_selected_notifications(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:        
            Notification.objects.filter(pk=pk,user=request.user).update(is_deleted=True,is_read=True)
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Notification(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('users:notifications')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@login_required
def read_selected_notifications(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:        
            Notification.objects.filter(pk=pk,user=request.user).update(is_deleted=True,is_read=True)
    
        response_data = {
            "status" : "true",
            "title" : "Successfully marked as read",
            "message" : "Selected notification(s) successfully marked as read.",
            "redirect" : "true",
            "redirect_url" : reverse('users:notifications')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@login_required
def read_notification(request,pk):
    Notification.objects.filter(pk=pk,user=request.user).update(is_read=True)
    
    response_data = {
        "status" : "true",
        "title" : "Successfully marked as read",
        "message" : "Notification successfully marked as read.",
        "redirect" : "true",
        "redirect_url" : reverse('users:notifications')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@login_required    
@ajax_required
@require_GET
def set_user_timezone(request): 
    timezone = request.GET.get('timezone')
    request.session["set_user_timezone"] = timezone
    response_data = {}
    response_data['status'] = 'true'
    response_data['title'] = "Success"  
    response_data['message'] = 'user timezone set successfully.'
    return HttpResponse(json.dumps(response_data), content_type='application/javascript') 