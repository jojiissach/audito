from django.conf.urls import url
import views


urlpatterns = [

    url(r'^create-user/(?P<pk>.*)/$', views.create_user, name='create_user'),
    # url(r'^view-user/(?P<pk>.*)/$', views.view_user, name='view_user'),
    # url(r'^edit-profile/$', views.edit_profile, name='edit_profile'),
    # url(r'^delete-user/(?P<pk>.*)/$', views.delete_user, name='delete_user'),
    # url(r'^delete-selected-users/$', views.delete_selected_users, name='delete_selected_users'),
    url(r'^change-permissions/(?P<pk>.*)/$', views.change_permissions, name='change_permissions'),
    # url(r'^change-password/(?P<pk>.*)/$', views.change_password, name='change_password'),
    # url(r'^view-users/$', views.users, name='users'),    
    # url(r'^settings/update-shop/$', views.update_shop, name='update_shop'),
    # url(r'^settings/update-profile/$', views.update_profile, name='update_profile'),
    # url(r'^settings/create-profile/$', views.create_profile, name='create_profile'),

    url(r'^check-notification/$', views.check_notification, name='check_notification'),
    url(r'^notifications/$', views.notifications, name='notifications'),
    url(r'^notification/delete/(?P<pk>.*)/$', views.delete_notification, name='delete_notification'),
    url(r'^delete-selected-notifications/$', views.delete_selected_notifications, name='delete_selected_notifications'),
    url(r'^notification/read/(?P<pk>.*)/$', views.read_notification, name='read_notification'),
    url(r'^read-selected-notifications/$', views.read_selected_notifications, name='read_selected_notifications'),
]