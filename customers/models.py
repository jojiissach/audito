from __future__ import unicode_literals
from django.db import models
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from decimal import Decimal
from versatileimagefield.fields import VersatileImageField
from main.models import BaseModel


class Customer(BaseModel):
    user = models.OneToOneField("auth.user",blank=True,null=True)
    name = models.CharField(max_length=128,blank=True,null=True)
    phone = models.CharField(max_length=128,null=True,blank=True)
    email = models.EmailField(null=True,blank=True)
    address = models.CharField(max_length=256,null=True,blank=True)
    district = models.CharField(max_length=128,null=True,blank=True)
    state = models.CharField(max_length=128,null=True,blank=True)
    zipcode = models.CharField(max_length=128,null=True,blank=True)
    photo = VersatileImageField(upload_to="uploads/customers/",blank=True,null=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'customers'
        verbose_name = _('customer')
        verbose_name_plural = _('customers')
        ordering = ('name',)
        
    class Admin:
        list_display = ('name',)

    def __unicode__(self):
        return self.title