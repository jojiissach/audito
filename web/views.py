from django.shortcuts import render, get_object_or_404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
import json
from users.functions import create_notification
from django.template.loader import render_to_string
from decimal import Decimal
from django.db.models import Q
from auditoriums.models import Auditorium
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode,role_required
from web.forms import RegisterForm, LoginForm
from django.contrib.auth import authenticate,login as auth_login,logout as auth_logout
from customers.models import Customer
from django.contrib.auth.models import User,Group
from main.functions import get_auto_id,generate_form_errors
from reservations.models import Reservation
import datetime
from web.forms import ReservationForm


def index(request):
	auditoriums = Auditorium.objects.filter(is_deleted=False)
	location = request.GET.get("slocation")
	date = request.GET.get("sdate")
	# search by location
	if location:
		auditoriums = auditoriums.filter(Q(location__icontains=location)|
									Q(address__icontains=location)|
									Q(city__icontains=location)|
									Q(district__icontains=location)|
									Q(state__icontains=location)
									)
	#search by date
	if date:
		date = datetime.datetime.strptime(date, '%d/%m/%Y')
		booked_auditorium_list = Reservation.objects.filter(Q(from_date=date)|Q(to_date=date)).values('auditorium')
		auditoriums = auditoriums.exclude(pk__in=booked_auditorium_list)
	context = {
		"title" : "Home",
		"auditoriums" : auditoriums,
	}
	return render(request,"web/index.html",context)


def single(request,pk):
	auditorium = get_object_or_404(Auditorium.objects.filter(pk=pk))
	context = {
		"title" : "Auditorium",
		"auditorium" : auditorium
	}
	return render(request,"web/single.html",context)


def customer_registration(request): 
	if request.method == "POST":
		form = RegisterForm(request.POST)
		response_data = {} 
		message = ""  
		data = None
		email = request.POST.get('email')
		email = str(email)
		username = request.POST.get('username')
		error = False
		password = request.POST.get('password1')
		if len(str(password)) < 8:
			error = True
			message += "This password is too small.It must contain 8 characters"
		if User.objects.filter(email=email).exists():
			error = True
			message += "This email already exists."
		if not error:
			if form.is_valid():  
				auto_id = get_auto_id(Customer)
				data = form.save(commit=False)
				data.email = email
				data.is_active = True
				data.save()
				instance = data

				Customer.objects.create(
					user=instance,
					email=instance.email,
					auto_id=auto_id,
					creator=instance,
					updator=instance,
				)                
				group = Group.objects.get(name="customer")
				instance.groups.add(group)
				username = form.cleaned_data['username']
				password = form.cleaned_data['password1']

				user = authenticate(username=username, password=password)
				
				auth_login(request, user)
				response_data = {
					'status' : 'true',
					"title" : "Account Created",
					"message" : "Successfully created account",
					'account_page' : 'true',
					"redirect" : 'true',
					'redirect_url' : reverse('web:index'),
				} 
			else:
				message = generate_form_errors(form, formset=False)
				response_data = {
					'status' : 'false',
					"title" : "Form Validation Error",
					"redirect" :'true',
					"message" : message,
					"stable" : 'true',
					'account_page' : 'true',
				}    
		else:
			response_data = {
				'status' : 'false',
				'stable' : 'true',
				"title" : "Error Occured",
				"message" : message,
				'account_page' : 'true',
			}
		return HttpResponse(json.dumps(response_data), content_type="application/javascript")
	else:
		form = RegisterForm()     
		context = {
			"title" : "Login",
			"form" : form,
		}
		return render(request, 'web/signup-form.html', context)


def customer_login(request):
	if request.method == "POST":
		login_form = LoginForm(request.POST)
		if login_form.is_valid():
			username = request.POST.get('username')
			password = request.POST.get('password')
			user = authenticate(username=username, password=password)
			if not user:
				
				user = authenticate(email=username,password=password)

			if not user:
				if not user:
					message = "Customer with this username and password does not exist."
				   
					response_data = {
						'status' : 'false',
						'stable' : 'true', 
						"redirect" : 'true',                   
						'redirect_url' : reverse('web:index'),
						'title' : "User not exists",
						"message" : message
					}
			else:
				if Customer.objects.filter(user=user).exists():
					auth_login(request, user)
					response_data = {
						'status' : 'true',
						"redirect" : 'true',
						"no-popup" : 'true',
						'redirect_url' : reverse('web:index'),
						'title' : "Successfully Logged in",
						"message" : "You are successfully Logged in as a client."
					}
				else:
					message = "Customer with this username and password does not exist."
					title = "Client not exists",
						
					response_data = {
						'status' : 'false',
						'stable' : 'true',
						"redirect" : 'true',
						'redirect_url' : reverse('web:index'),
						'title' : title,
						"message" : message
					}
		else:
			username = request.POST.get('username')
			message = ""
			if not username:
				message += "Email field is required "

			password = request.POST.get('password')
			if not password:
				message += "| password field is required"
			response_data = {
				'status' : 'false',
				'stable' : 'false',
				'title' : "Invalid Credential",
				"message" : message,
				"redirect" : 'false',
			}
		return HttpResponse(json.dumps(response_data), content_type='application/javascript')
	else:
		return HttpResponseRedirect(reverse('web:index'))


@login_required(login_url='/login/')
def logout(request):
	auth_logout(request)
	return HttpResponseRedirect(reverse('web:index'))


@check_mode
@login_required
@role_required(['customer',])
def book_now(request,pk):
	instance = get_object_or_404(Auditorium.objects.filter(is_deleted=False,pk=pk))
	if request.method == 'POST':
		form = ReservationForm(request.POST)
		
		if form.is_valid():
			customer = get_object_or_404(Customer.objects.filter(is_deleted=False,user=request.user))

			data = form.save(commit=False)
			data.creator = request.user
			data.updator = request.user
			data.auto_id = get_auto_id(Reservation)
			data.auditorium = instance
			data.customer = customer
			data.status = 'booked'
			data.save() 

			response_data = {
				"status" : "true",
				"title" : "Successfully Booked",
				"message" : "Reservation(s) created successfully.",
				"redirect" : "true",
				"redirect_url" : reverse('web:reservations')
			}

		else:
			message = generate_form_errors(form, formset=False)
			response_data = {
				"status" : "false",
				"stable" : "true",
				"title" : "Form validation error",
				"message" : message
			}   
		return HttpResponse(json.dumps(response_data), content_type='application/javascript')
	else:
		form = ReservationForm()

		context = {
			"title": "Create Reservation",
			"url": reverse('web:book_now',kwargs={'pk':instance.pk}),
			"form" : form,
			"redirect" : True,

			"is_need_popup_box": True,
			"is_need_grid_system": True,
			"is_need_animations": True,
			"is_need_datetime_picker": True,
			"is_need_checkbox": True,
			"is_need_select_picker" : True,
			"is_need_formset" : True,
		}
		return render(request,'web/reservation_entry.html',context)


def reservations(request):
	instances = Reservation.objects.filter(is_deleted=False)
	title = "reservations"

	query = request.GET.get("q")
	if query:
		instances = instances.filter(Q(name__icontains=query))
		title = "reservations - %s" % query

	context = {
		"instances": instances,
		'title': title,

		"is_need_popup_box": True,
		"is_need_grid_system": True,
		"is_need_animations": True,
		"is_need_datetime_picker": True,
		"is_need_checkbox": True
	}
	return render(request, 'web/reservations.html', context)


def print_reservation(request, pk):
	instance = get_object_or_404(Reservation.objects.filter(is_deleted=False,pk=pk))
	title = "Reservation"

	context = {
		"instance": instance,
		'title': title,

		"is_need_popup_box": True,
		"is_need_grid_system": True,
		"is_need_animations": True,
		"is_need_datetime_picker": True,
		"is_need_checkbox": True
	}
	return render(request, 'web/print_reservation.html', context)


def check_availabilty(request): 
	today = datetime.date.today()
	if request.method == "POST":
		date = request.POST.get("sdate")
		
		#check by date
		available = True
		if date:
			date = datetime.datetime.strptime(date, '%d/%m/%Y')
			if Reservation.objects.filter(Q(from_date=date)|Q(to_date=date)).exists():
				available = False
			if available:
				response_data = {
						"status" : "true",
						"stable" : "true",
						"title" : "Available",
						"message" : "Auditorium avilable for this date"
					}
			else:
				response_data = {
						"status" : "false",
						"stable" : "true",
						"title" : "Not Available",
						"message" : "Auditorium not avilable for this date "
					}
		else:
			response_data = {
						"status" : "false",
						"stable" : "true",
						"title" : "Select Date",
						"message" : "Invalid Date selection"
					}
		return HttpResponse(json.dumps(response_data), content_type="application/javascript")