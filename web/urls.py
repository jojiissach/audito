from django.conf.urls import url, include
import views

urlpatterns = [               

    url(r'^$', views.index,name='index'), 
   
    url(r'^single/(?P<pk>.*)/$', views.single,name='single'),
   	url(r'^customer-registration/$', views.customer_registration, name='customer_registration'), 
    url(r'^login/$', views.customer_login, name='customer_login'),
    url(r'^customer/logout/$', views.logout, name='logout'),

    url(r'^book-now/(?P<pk>.*)/$', views.book_now,name='book_now'),
    url(r'^booking-list/$', views.reservations, name='reservations'),
    url(r'^print-booking/(?P<pk>.*)/$', views.print_reservation,name='print_reservation'),
    url(r'^check-availabilty/$', views.check_availabilty, name='check_availabilty'), 
    
]