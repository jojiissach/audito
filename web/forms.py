from django import forms
from django.forms.widgets import TextInput, Textarea, Select
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import UsernameField
from registration.forms import RegistrationForm
from django.contrib.auth.models import User
from reservations.models import Reservation


class RegisterForm(RegistrationForm):
    
    username = forms.CharField(label=_("Username"), 
                               max_length=254,
                               widget=forms.TextInput(
                                    attrs={'class':'required form-control','placeholder' : 'Username'})
                               )

    email = forms.EmailField(label=_("Email"), 
                             max_length=254,
                             widget=forms.TextInput(
                                attrs={'class':'required form-control'})
                             )

    password1 = forms.CharField(label=_("Password"), 
                               widget=forms.PasswordInput(
                                    attrs={'class':'required form-control'})
                               )

    password2 = forms.CharField(label=_("Repeat Password"), 
                               widget=forms.PasswordInput(
                                    attrs={'class':'required form-control'})
                               )

    bad_domains = ['example.com']
    def clean_email(self):
        email_domain = self.cleaned_data['email'].split('@')[1]
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_("This email address is already in use."))
        elif email_domain in self.bad_domains:

            raise forms.ValidationError(_("Registration using %s email addresses is not allowed. Please supply a different email address." %email_domain))
        return self.cleaned_data['email']

    min_password_length = 6

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1', '')
        if len(password1) < self.min_password_length:
            raise forms.ValidationError("Password must have at least %i characters" % self.min_password_length)
        else:
            return password1

    def clean_password2(self):
        print 'hi'
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    min_username_length = 5

    def clean_username(self):
        username = self.cleaned_data['username']
        existing = User.objects.filter(username__iexact=self.cleaned_data['username'])
        if existing.exists():
            raise forms.ValidationError(_("A user with that username already exists."))
        elif len(username) < self.min_username_length:
            raise forms.ValidationError("Username must have at least %i characters" % self.min_password_length)
        else:
            return self.cleaned_data['username']


class LoginForm(forms.Form):

    username = UsernameField(
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True,'placeholder':"Username",'class' : 'required'}),
    )
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={'placeholder':"Password",'class' : 'required'}),
    )

    error_messages = {
        'invalid_login': _(
            "Please enter a correct %(username)s and password. Note that both "
            "fields may be case-sensitive."
        ),
        'inactive': _("This account is inactive."),

        'username': {
            'required': _("email field is required."),
        },
        'password': {
            'required': _("Passworld field is required."),
        }
    }
    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache

    def clean(self):
        cleaned_data=super(LoginForm, self).clean()
        username = self.cleaned_data.get('username')
        if not username:
            raise forms.ValidationError("Username must have at least  characters")
        return self.cleaned_data


class ReservationForm(forms.ModelForm):

    class Meta:
        model = Reservation
        exclude = ['creator','updator','auto_id','is_deleted','auditorium','amount','customer','status']
        widgets = {
            'from_date': TextInput(attrs={'class': 'required form-control datepicker','placeholder' : 'From'}),
            'to_date': TextInput(attrs={'class': 'form-control datepicker','placeholder' : 'To'}),
            'client_name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}),
            'client_address': TextInput(attrs={'class': 'required form-control','placeholder' : 'Location'}),
            'city': TextInput(attrs={'class': 'required form-control','placeholder' : 'City'}),
            'zipcode': TextInput(attrs={'class': 'required form-control','placeholder' : 'ZIP'}),
            'phone': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Phone'}),
            'email': TextInput(attrs={'class': 'required form-control','placeholder' : 'Email'}),
            
        }

        error_messages = {
            'name' : {
                'required' : _("Product field is required."),
            },
            'Address' : {
                'required' : _("Offer percentage field is required."),
            },
            'district' : {
                'required' : _("Start date field is required."),
            },
            'state' : {
                'required' : _("End Date field is required."),
            },
            'contact1' : {
                'required' : _("End Date field is required."),
            },
            'contact2' : {
                'required' : _("End Date field is required."),
            },
            'zipcode' : {
                'required' : _("End Date field is required."),
            },
        }
