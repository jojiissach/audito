# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from decimal import Decimal
from versatileimagefield.fields import VersatileImageField
from main.models import BaseModel

STATUS = (
    ('booked', 'Booked'),
    ('cancelled', 'Cancelled'),
    ('completed', 'Completed'),
)


class Reservation(BaseModel):
    auditorium = models.ForeignKey("auditoriums.Auditorium")
    customer = models.ForeignKey("customers.Customer",null=True,blank=True)
    from_date = models.DateTimeField()
    to_date = models.DateTimeField(null=True,blank=True) 
    client_name = models.CharField(max_length=256)
    client_address = models.CharField(max_length=128)
    city = models.CharField(max_length=128)
    zipcode = models.CharField(max_length=128)
    phone = models.CharField(max_length=128)
    email = models.EmailField(null=True,blank=True)
    amount = models.DecimalField(default=0.00,blank=True,decimal_places=2, max_digits=15, validators=[MinValueValidator(Decimal('0.00'))])
    is_deleted = models.BooleanField(default=False)
    status = models.CharField(max_length=128,choices=STATUS)

    class Meta:
        db_table = 'reservations'
        verbose_name = _('reservation')
        verbose_name_plural = _('reservations')
        ordering = ('id',)
        
    class Admin:
        list_display = ('id',)

    def __unicode__(self):
        return self.title