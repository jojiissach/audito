from django.utils.translation import ugettext_lazy as _
from django import forms
from django.forms.widgets import TextInput,Textarea,Select,DateInput, CheckboxInput
from django.contrib.auth.models import User
from reservations.models import Reservation
from dal import autocomplete


class ReservationForm(forms.ModelForm):

	class Meta:
		model = Reservation
		exclude = ['creator','updator','auto_id','is_deleted','amount','customer','status']
		widgets = {
			'auditorium': autocomplete.ModelSelect2(url='auditoriums:auditorium_autocomplete', attrs={'data-placeholder': 'Auditorium', 'data-minimum-input-length': 1},), 
			'from_date': TextInput(attrs={'class': 'required form-control date-picker','placeholder' : 'From Date'}),
			'to_date': TextInput(attrs={'class': 'form-control date-picker','placeholder' : 'To Date'}),
			'client_name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}),
			'client_address': TextInput(attrs={'class': 'required form-control','placeholder' : 'Address'}),
			'city': TextInput(attrs={'class': 'required form-control','placeholder' : 'City'}),
			'zipcode': TextInput(attrs={'class': 'required form-control','placeholder' : 'ZIP'}),
			'phone': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Phone'}),
			'email': TextInput(attrs={'class': 'required form-control','placeholder' : 'Email'}),
			
		}

		error_messages = {
			'name' : {
				'required' : _("Product field is required."),
			},
			'Address' : {
				'required' : _("Offer percentage field is required."),
			},
			'district' : {
				'required' : _("Start date field is required."),
			},
			'state' : {
				'required' : _("End Date field is required."),
			},
			'contact1' : {
				'required' : _("End Date field is required."),
			},
			'contact2' : {
				'required' : _("End Date field is required."),
			},
			'zipcode' : {
				'required' : _("End Date field is required."),
			},
		}
