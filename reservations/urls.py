from django.conf.urls import url, include
from django.contrib import admin
import views


urlpatterns = [               
    url(r'^reservations/create/$',views.create_reservation,name='create_reservation'),
    url(r'^reservations/view/$',views.reservations,name='reservations'),
    url(r'^reservations/delete/(?P<pk>.*)/$',views.delete_reservation,name='delete_reservation'),
    url(r'^reservations/cancel/(?P<pk>.*)/$',views.cancel_reservation,name='cancel_reservation'),
    
]