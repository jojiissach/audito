# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render,get_object_or_404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from reservations.models import Reservation
from reservations.forms import ReservationForm
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode,role_required
from main.functions import get_auto_id,generate_form_errors
import json
import datetime


# Create your views here.
@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def create_reservation(request):

    if request.method == 'POST':
        form = ReservationForm(request.POST,request.FILES)
        
        if form.is_valid():


            data = form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.auto_id = get_auto_id(Reservation)
            data.status = 'booked'
            data.save() 

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Reservation(s) created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('reservations:reservations')
            }

        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = ReservationForm()

        context = {
            "title": "Create Reservation",
            "url": reverse('reservations:create_reservation'),
            "form" : form,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_grid_system": True,
            "is_need_animations": True,
            "is_need_datetime_picker": True,
            "is_need_checkbox": True,
            "is_need_select_picker" : True,
            "is_need_formset" : True,
        }
        return render(request,'reservations/entry.html',context)


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def reservations(request):
    instances = Reservation.objects.filter(is_deleted=False)
    title = "reservations"

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query))
        title = "reservations - %s" % query

    context = {
        "instances": instances,
        'title': title,

        "is_need_popup_box": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
        "is_need_checkbox": True
    }
    return render(request, 'reservations/reservations.html', context)


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def delete_reservation(request,pk):
    instance = get_object_or_404(Reservation.objects.filter(pk=pk,is_deleted=False))
    
    Reservation.objects.filter(pk=pk).update(is_deleted=True)
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Reservation Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('reservations:reservations')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def cancel_reservation(request,pk):
    instance = get_object_or_404(Reservation.objects.filter(pk=pk,is_deleted=False))
    
    Reservation.objects.filter(pk=pk).update(status="cancelled")
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Cancelled",
        "message" : "Reservation Successfully Cancelled.",
        "redirect" : "true",
        "redirect_url" : reverse('reservations:reservations')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')