from django.conf.urls import url,include
from django.contrib import admin
from django.views.static import serve
from main import views as general_views
from django.conf import settings
from registration.backends.default.views import RegistrationView
from users.forms import RegForm
from users.backend import user_created
from django.contrib.auth import views as auth_views


urlpatterns = [
    url(r'^admin', admin.site.urls),

    url(r'^', include('web.urls', namespace="web")),
    url(r'^app/$',general_views.app,name='app'),
    url(r'^report/$',general_views.report,name='report'),
    url(r'^app/dashboard/$',general_views.dashboard,name='dashboard'),
    url(r'^app/auditorium/', include('auditoriums.urls', namespace="auditoriums")),
    url(r'^app/partner/', include('partners.urls', namespace="partners")),
    url(r'^app/resrvation/', include('reservations.urls', namespace="reservations")),

    url(r'^app/accounts/register/$', RegistrationView.as_view(form_class=RegForm),name='registration_register'),
    url(r'^app/accounts/', include('registration.backends.default.urls')), 

    url(r'^media/(?P<path>.*)$', serve, { 'document_root': settings.MEDIA_ROOT}), 
    url(r'^static/(?P<path>.*)$', serve, { 'document_root': settings.STATIC_FILE_ROOT}),
]
