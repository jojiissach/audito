# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render,get_object_or_404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from partners.models import Partner
from partners.forms import PartnerForm
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode,role_required
from main.functions import get_auto_id,generate_form_errors
import json
from users.forms import UserForm
from django.contrib.auth.models import User,Group
from dal import autocomplete
from django.db.models import Q
from main.functions import get_current_role


class PartnerAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = []
        current_role = get_current_role(self.request)
        if current_role == 'partner':
            items = Partner.objects.filter(is_deleted=False,user=self.request.user)
        else:
            items = Partner.objects.filter(is_deleted=False)
        if self.q:
            partners = items.filter(Q(name__istartswith=self.q))

    
        return partners


# Create your views here.
@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def create_partner(request):

    if request.method == 'POST':
        form = PartnerForm(request.POST,request.FILES)
        
        if form.is_valid():


            data = form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.auto_id = get_auto_id(Partner)
            data.save() 

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Partner(s) created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('partners:partners')
            }

        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = PartnerForm()

        context = {
            "title": "Create Partner",
            "url": reverse('partners:create_partner'),
            "form" : form,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_grid_system": True,
            "is_need_animations": True,
            "is_need_datetime_picker": True,
            "is_need_checkbox": True,
            "is_need_select_picker" : True,
            "is_need_formset" : True,
        }
        return render(request,'partners/entry.html',context)


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def edit_partner(request,pk):
    instance = get_object_or_404(Partner.objects.filter(pk=pk,is_deleted=False)) 
    
    if request.method == 'POST':
            
        response_data = {}
        form = PartnerForm(request.POST,request.FILES,instance=instance)
        
        if form.is_valid():  
            
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()  


            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Partner Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('partners:Partner',kwargs={'pk':data.pk})
            }   
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 

        form = PartnerForm(instance=instance)
        active_pincodes = instance.pincodes.all()
        
        context = {
            "form" : form,
            "title" : "Edit Partner : " + instance.name,
            "instance" : instance,
            "url" : reverse('partners:edit_partner',kwargs={'pk':instance.pk}),
            "redirect" : True,
            "active_pincodes" : active_pincodes,
            "cities" : cities,

            "is_need_popup_box": True,
            "is_need_grid_system": True,
            "is_need_animations": True,
            "is_need_datetime_picker": True,
            "is_need_checkbox": True,
            "is_need_select_picker" : True,
        }
        return render(request, 'partners/entry_partner.html', context)


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def partners(request):
    instances = Partner.objects.filter(is_deleted=False)
    title = "Partners"

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query))
        title = "partners - %s" % query

    context = {
        "instances": instances,
        'title': title,

        "is_need_popup_box": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
        "is_need_checkbox": True
    }
    return render(request, 'partners/partners.html', context)


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def partner(request, pk):
    instance = get_object_or_404(Partner.objects.filter(is_deleted=False,pk=pk))
    title = "Partner"

    context = {
        "instance": instance,
        'title': title,

        "is_need_popup_box": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
        "is_need_checkbox": True
    }
    return render(request, 'partners/partner.html', context)


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def delete_partner(request,pk):
    instance = get_object_or_404(Partner.objects.filter(pk=pk,is_deleted=False))
    
    Partner.objects.filter(pk=pk).update(is_deleted=True)
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Partner Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('partners:partners')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def delete_selected_partners(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Partner.objects.filter(pk=pk,is_deleted=False)) 
            Partner.objects.filter(pk=pk).update(is_deleted=True)
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Partner(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('partners:partners')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@login_required
@role_required(['superadmin','administrator'])
def make_user(request,pk): 
    partner = get_object_or_404(Partner.objects.filter(pk=pk))

    if request.method == "POST":
        form = UserForm(request.POST)
        response_data = {} 
        message = ""  
        data = []  
        emails = request.POST.get('email')
        email = str(emails)
        error = False
        if User.objects.filter(email=email).exists():
            error = True
            message += "This email already exists."
        if partner.user:
            error = True
            message += "This partner already has a user."
    
        if not error:
            if form.is_valid():    

                data = form.save(commit=False)
                data.email = email
                data.save()

                instance = data
                group = Group.objects.get(name="partner")
                instance.groups.add(group)

                partner.user=data
                partner.save()

                response_data = {
                    'status' : 'true',     
                    'title' : "User Created",       
                    'redirect' : 'true', 
                    'redirect_url' : reverse('partners:partner', kwargs = {'pk' : pk}),
                    'message' : "User Created Successfully"
                }
    
            else:
                error = True
                message = ''            
                message += generate_form_errors(form,formset=False)
                response_data = {
                    'status' : 'false',
                    'stable' : 'true',
                    'title' : "Form validation error",
                    'message' : message
                }
                
                message += generate_form_errors(form,formset=False)
    
        else:
            response_data = {
                'status' : 'false',     
                'title' : "Can't create this user",       
                'redirect' : 'true', 
                'redirect_url' : reverse('partners:make_user', kwargs={"pk":pk}),
                'message' : message
            }
    
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 
        form = UserForm(initial={'email':partner.email})
        
        context = {
            "form" : form,
            "title" : "Create User",
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_grid_system": True,
            "is_need_animations": True,
            "is_need_datetime_picker": True,
            "is_need_checkbox" : True
        }
    return render(request, 'users/create_user.html', context)

