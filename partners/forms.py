from django.utils.translation import ugettext_lazy as _
from django import forms
from django.forms.widgets import TextInput,Textarea,Select,DateInput, CheckboxInput
from django.contrib.auth.models import User
from partners.models import Partner


class PartnerForm(forms.ModelForm):

    class Meta:
        model = Partner
        exclude = ['creator','updator','auto_id','is_deleted']
        widgets = {
           'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}),
           'address': TextInput(attrs={'class': 'required form-control','placeholder' : 'Address'}),
           'district': TextInput(attrs={'class': 'required form-control','placeholder' : 'District'}),
           'state': TextInput(attrs={'class': 'required form-control','placeholder' : 'State'}),
           'zipcode': TextInput(attrs={'class': 'required form-control','placeholder' : 'ZIP'}),
           'contact1': TextInput(attrs={'class': 'required form-control','placeholder' : 'Contact 1'}),
           'contact2': TextInput(attrs={'class': 'required form-control','placeholder' : 'Contact 2'}),
           'email': TextInput(attrs={'class': 'required form-control','placeholder' : 'Email'}),
           
        }
        error_messages = {
            'name' : {
                'required' : _("Product field is required."),
            },
            'Address' : {
                'required' : _("Offer percentage field is required."),
            },
            'district' : {
                'required' : _("Start date field is required."),
            },
            'state' : {
                'required' : _("End Date field is required."),
            },
            'contact1' : {
                'required' : _("End Date field is required."),
            },
            'contact2' : {
                'required' : _("End Date field is required."),
            },
            'zipcode' : {
                'required' : _("End Date field is required."),
            },
        }
