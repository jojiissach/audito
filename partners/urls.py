from django.conf.urls import url, include
from django.contrib import admin
import views
from views import PartnerAutocomplete


urlpatterns = [               
	url(r'^partner-autocomplete/$',PartnerAutocomplete.as_view(),name='partner_autocomplete'), 
    url(r'^partners/create/$',views.create_partner,name='create_partner'),
    url(r'^partners/view/$',views.partners,name='partners'),
    url(r'^partners/edit/(?P<pk>.*)/$',views.edit_partner,name='edit_partner'),
    url(r'^partners/view/(?P<pk>.*)/$',views.partner,name='partner'),
    url(r'^partners/delete/(?P<pk>.*)/$',views.delete_partner,name='delete_partner'),
    url(r'^partners/delete-selected-partners/$', views.delete_selected_partners, name='delete_selected_partners'),

    url(r'^partners/make-user/(?P<pk>.*)/$',views.make_user,name='make_user'),
]