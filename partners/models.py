from __future__ import unicode_literals
from django.db import models
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from decimal import Decimal
from versatileimagefield.fields import VersatileImageField
from main.models import BaseModel


class Partner(BaseModel):
    user = models.OneToOneField("auth.user",blank=True,null=True)
    name = models.CharField(max_length=128)
    address = models.CharField(max_length=256)
    district = models.CharField(max_length=128)
    state = models.CharField(max_length=128)
    zipcode = models.CharField(max_length=128)
    contact1 = models.CharField(max_length=128)
    contact2 = models.CharField(max_length=128)
    email = models.EmailField(null=True,blank=True)
    photo = VersatileImageField(upload_to="uploads/partners/",blank=True,null=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'partners'
        verbose_name = _('partner')
        verbose_name_plural = _('partners')
        ordering = ('name',)
        
    class Admin:
        list_display = ('name',)

    def __unicode__(self):
        return self.name