import os
import string
import random
from cgi import escape
from django.template.loader import get_template
from django.template import Context
import cStringIO as StringIO
from django.http import HttpResponse
from django.conf import settings
from decimal import Decimal
from django.contrib.auth.models import User


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def generate_unique_id(size=8, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def generate_form_errors(args,formset=False):
    message = ''
    if not formset:
        for field in args:
            if field.errors:
                message += field.errors  + "|"
        for err in args.non_field_errors():
            message += str(err) + "|"
                
    elif formset:
        for form in args:
            for field in form:
                if field.errors:
                    message +=field.errors + "|"
            for err in form.non_field_errors():
                message += str(err) + "|"
    return message


def fetch_resources(uri, rel):
    import os.path
    path = os.path.join(settings.STATIC_FILE_ROOT,uri.replace(settings.STATIC_URL, ""))
    return path


def render_to_pdf(template_src, context_dict):
    template = get_template(template_src)
    context = Context(context_dict)
    html  = template.render(context)
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("ISO-8859-1")), result,link_callback=fetch_resources)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return HttpResponse('We had some errors<pre>%s</pre>' % escape(html))


def get_auto_id(model):
    auto_id = 1 
    latest_auto_id =  model.objects.all().order_by("-date_added")[:1]
    if latest_auto_id:
        for auto in latest_auto_id:
            auto_id = auto.auto_id + 1
    return auto_id


def get_a_id(model,request):
    a_id = 1 
    current_shop = get_current_shop(request)
    latest_a_id =  model.objects.filter(shop=current_shop).order_by("-date_added")[:1]
    if latest_a_id:
        for auto in latest_a_id:
            a_id = auto.a_id + 1
    return a_id


def get_timezone(request):
    if "set_user_timezone" in request.session:
        user_time_zone = request.session['set_user_timezone']
    else:
        user_time_zone = "Asia/Kolkata"
    return user_time_zone


def get_account_balance(request,user):
    balance = 0
    if request.user.is_authenticated():
        if AccountBalance.objects.filter(user=user).exists():
            balance = user.accountbalance.balance
    
    return balance


def get_free_amount(request):
    return Decimal(270)


def daily_rate():
    return Decimal(9)


def get_low_balance_limit():
    return Decimal(90)


def get_currency(user=None):
    return "Rs"


def minimum_amount():
    return Decimal(250)


def invite_code_amount():
    return Decimal(270)


def promo_code_amount():
    return Decimal(135)


def get_roles(request):    
    roles = []
    if request.user.is_authenticated():
        current_shop = get_current_shop(request)
        shop_access = ShopAccess.objects.filter(shop=current_shop,user=request.user,is_accepted=True)
        for access in shop_access:
            roles.append(access.group.name)
        
    return roles


def get_current_role(request):
    is_superadmin = False
    is_administrator = False
    is_partner = False
    is_customer = False
    
    if request.user.is_authenticated():        
        
        if User.objects.filter(id=request.user.id,is_superuser=True,is_active=True).exists():
            is_superadmin = True
        
        if User.objects.filter(id=request.user.id,is_active=True,groups__name="administrator").exists():
            is_administrator = True
            
        if User.objects.filter(id=request.user.id,is_active=True,groups__name="partner").exists():
            is_partner = True
            
        if User.objects.filter(id=request.user.id,is_active=True,groups__name="customer").exists():
            is_customer = True
 
    current_role = "user"    
    
    if is_superadmin:
        current_role = "superadmin"
    elif is_administrator:
        current_role = "administrator"
    elif is_partner:
        current_role = "partner"
    elif is_customer:
        current_role = "customer"
                
    return current_role


def get_shops(request):
    shops = []
    if request.user.is_authenticated():
        shop_access = ShopAccess.objects.filter(user=request.user,is_accepted=True)
        for access in shop_access:
            if not access.shop in shops:
                shops.append(access.shop)
        
    return shops