from django.shortcuts import render
from django.http.response import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode,role_required
import json
from django.contrib.auth.models import Group
import datetime 
from calendar import monthrange
from main.functions import get_current_role


@check_mode
@login_required
def app(request):
    return HttpResponseRedirect(reverse('dashboard'))


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def dashboard(request):
    today = datetime.date.today()
                          
    context = {
        "title" : "Dashboard",
        "today" : today,       
        
        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
        "is_dashboard" :True,
        "is_need_datepicker" : True,

    }
    return render(request,"base.html",context)


@login_required
@role_required(['supplier','superadmin','administrator'])
def report(request):
    current_role = get_current_role(request)
    today = datetime.date.today()
    period = request.GET.get('period')

    product_added_count = 0
    pending_orders = 0
    cancelled_orders = 0
    delivered_orders = 0
    new_orders_count = 0
    supplier_payment_value = 0
    total_income = 0
    this_period_customers_count = 0

    income_amounts = [0,0,0,0,0,0,0,0,0,0,0,0]
    expense_amounts = [0,0,0,0,0,0,0,0,0,0,0,0]
    order_counts = [0,0,0,0,0,0,0,0,0,0,0,0]


    if period == "monthly":
        pass;
    
    elif period == "today":    
        pass;

    

    result = {
        "product_added_count" : 0,
        "supplier_payment_value" : 0,
        "new_orders_count" : 0,
        "total_income" : 0,
        "income_amounts" : 0,
        "expense_amounts" : 0,
        "order_counts" : 0,
        "cancelled_orders" : 0,
        "delivered_orders" : 0,
        "pending_orders" : 0,
        "this_period_customers_count" : 0
    }

    return HttpResponse(json.dumps(result),content_type='text/plain')