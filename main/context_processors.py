from main.functions import get_account_balance,get_current_role
from main.models import Mode
import datetime
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from web.forms import RegisterForm


def main_context(request):
    today = datetime.date.today()
    current_role = get_current_role(request)
    super_users = User.objects.filter(is_superuser=True,is_active=True)
    is_superuser = False
    if "set_user_timezone" in request.session:
        user_session_ok = True
        user_time_zone = request.session['set_user_timezone']
    else:
        user_session_ok = False
        user_time_zone = "Asia/Kolkata"
    registration_form = RegisterForm()
    user_logined = False
    if request.user.is_authenticated():
        user_logined = True
    return {
        'app_title' : "Arkboss",
        "user_session_ok" : user_session_ok,
        "user_time_zone" : user_time_zone,
        "super_users" : super_users,
        "current_role" : current_role,
        "registration_form" : registration_form,
        "confirm_delete_message" : "Are you sure want to delete this item. All associated data may be removed.",
        "confirm_cancel_message" : "Are you sure want to cancel this booking.",
        "revoke_access_message" : "Are you sure to revoke this user's login access",
        "confirm_shop_delete_message" : "Your shop will deleted permanantly. All data will lost.",
        "confirm_delete_selected_message" : "Are you sure to delete all selected items.",
        "confirm_read_message" : "Are you sure want to mark as read this item.",
        "confirm_read_selected_message" : "Are you sure to mark as read all selected items.",
        'domain' : request.META['HTTP_HOST'],
        "user_logined" : user_logined,
    }
    
    





