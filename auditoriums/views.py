# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render,get_object_or_404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from auditoriums.models import Auditorium
from auditoriums.forms import AuditoriumForm
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode,role_required
from main.functions import get_auto_id,generate_form_errors
import json
import datetime
from dal import autocomplete
from django.db.models import Q
from main.functions import get_current_role


# Create your views here.
class AuditoriumAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        items = []
        current_role = get_current_role(self.request)
        if current_role == 'partner':
            items = Auditorium.objects.filter(is_deleted=False,partner__user=self.request.user)
        else:
            items = Auditorium.objects.filter(is_deleted=False)
        if self.q:
            auditoriums = items.filter(Q(name__istartswith=self.q))

    
        return auditoriums


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def create_auditorium(request):

    if request.method == 'POST':
        form = AuditoriumForm(request.POST,request.FILES)
        
        if form.is_valid():


            data = form.save(commit=False)
            data.creator = request.user
            data.updator = request.user
            data.auto_id = get_auto_id(Auditorium)
            data.save() 

            response_data = {
                "status" : "true",
                "title" : "Successfully Created",
                "message" : "Auditorium(s) created successfully.",
                "redirect" : "true",
                "redirect_url" : reverse('auditoriums:auditoriums')
            }

        else:
            message = generate_form_errors(form, formset=False)
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }   
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')
    else:
        form = AuditoriumForm()

        context = {
            "title": "Create Auditorium",
            "url": reverse('auditoriums:create_auditorium'),
            "form" : form,
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_grid_system": True,
            "is_need_animations": True,
            "is_need_datetime_picker": True,
            "is_need_checkbox": True,
            "is_need_select_picker" : True,
            "is_need_formset" : True,
        }
        return render(request,'auditoriums/entry.html',context)


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def edit_auditorium(request,pk):
    instance = get_object_or_404(Auditorium.objects.filter(pk=pk,is_deleted=False)) 
    
    if request.method == 'POST':
            
        response_data = {}
        form = AuditoriumForm(request.POST,request.FILES,instance=instance)
        
        if form.is_valid():  
            
            data = form.save(commit=False)
            data.updator = request.user
            data.date_updated = datetime.datetime.now()
            data.save()  

           

            response_data = {
                "status" : "true",
                "title" : "Successfully Updated",
                "message" : "Auditorium Successfully Updated.",
                "redirect" : "true",
                "redirect_url" : reverse('auditoriums:auditorium',kwargs={'pk':data.pk})
            }   
        else:
            message = generate_form_errors(form,formset=False)     
                    
            response_data = {
                "status" : "false",
                "stable" : "true",
                "title" : "Form validation error",
                "message" : message
            }  
            
        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else: 

        form = AuditoriumForm(instance=instance)
        
        context = {
            "form" : form,
            "title" : "Edit Auditorium : " + instance.name,
            "instance" : instance,
            "url" : reverse('auditoriums:edit_auditorium',kwargs={'pk':instance.pk}),
            "redirect" : True,

            "is_need_popup_box": True,
            "is_need_grid_system": True,
            "is_need_animations": True,
            "is_need_datetime_picker": True,
            "is_need_checkbox": True,
            "is_need_select_picker" : True,
        }
        return render(request, 'auditoriums/entry.html', context)


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def auditoriums(request):
    instances = Auditorium.objects.filter(is_deleted=False)
    title = "Auditoriums"

    query = request.GET.get("q")
    if query:
        instances = instances.filter(Q(name__icontains=query))
        title = "Auditoriums - %s" % query

    context = {
        "instances": instances,
        'title': title,

        "is_need_popup_box": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
        "is_need_checkbox": True
    }
    return render(request, 'auditoriums/auditoriums.html', context)


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def auditorium(request, pk):
    instance = get_object_or_404(Auditorium.objects.filter(is_deleted=False,pk=pk))
    title = "Auditorium"

    context = {
        "instance": instance,
        'title': title,

        "is_need_popup_box": True,
        "is_need_grid_system": True,
        "is_need_animations": True,
        "is_need_datetime_picker": True,
        "is_need_checkbox": True
    }
    return render(request, 'auditoriums/auditorium.html', context)


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def delete_auditorium(request,pk):
    instance = get_object_or_404(Auditorium.objects.filter(pk=pk,is_deleted=False))
    
    Auditorium.objects.filter(pk=pk).update(is_deleted=True)
    
    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Auditorium Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('auditoriums:auditoriums')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


@check_mode
@login_required
@role_required(['partner','superadmin','administrator'])
def delete_selected_auditoriums(request):
    pks = request.GET.get('pk')
    if pks:
        pks = pks[:-1]
        
        pks = pks.split(',')
        for pk in pks:      
            instance = get_object_or_404(Auditorium.objects.filter(pk=pk,is_deleted=False)) 
            Auditorium.objects.filter(pk=pk).update(is_deleted=True)
    
        response_data = {
            "status" : "true",
            "title" : "Successfully Deleted",
            "message" : "Selected Auditorium(s) Successfully Deleted.",
            "redirect" : "true",
            "redirect_url" : reverse('auditoriums:auditoriums')
        }
    else:
        response_data = {
            "status" : "false",
            "title" : "Nothing selected",
            "message" : "Please select some items first.",
        }
        
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')
