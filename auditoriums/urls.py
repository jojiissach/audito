from django.conf.urls import url, include
from django.contrib import admin
import views
from views import AuditoriumAutocomplete


urlpatterns = [               
	url(r'^partner-autocomplete/$',AuditoriumAutocomplete.as_view(),name='auditorium_autocomplete'), 
    url(r'^auditoriums/create/$',views.create_auditorium,name='create_auditorium'),
    url(r'^auditoriums/view/$',views.auditoriums,name='auditoriums'),
    url(r'^auditoriums/edit/(?P<pk>.*)/$',views.edit_auditorium,name='edit_auditorium'),
    url(r'^auditoriums/view/(?P<pk>.*)/$',views.auditorium,name='auditorium'),
    url(r'^auditoriums/delete/(?P<pk>.*)/$',views.delete_auditorium,name='delete_auditorium'),
    url(r'^auditoriums/delete-selected-auditoriums/$', views.delete_selected_auditoriums, name='delete_selected_auditoriums'), 
]