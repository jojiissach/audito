from django.contrib import admin
from auditoriums.models import Auditorium


# Registers the models on the Django Admin page
admin.site.register(Auditorium)