from __future__ import unicode_literals
from django.db import models
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from decimal import Decimal
from versatileimagefield.fields import VersatileImageField
from main.models import BaseModel


class Auditorium(BaseModel):
    partner = models.ForeignKey("partners.Partner",blank=True,null=True)
    name = models.CharField(max_length=128)
    title = models.CharField(max_length=128)
    address = models.CharField(max_length=256)
    location = models.CharField(max_length=128)
    city = models.CharField(max_length=128)
    district = models.CharField(max_length=128)
    state = models.CharField(max_length=128)
    zipcode = models.CharField(max_length=128)
    contact1 = models.CharField(max_length=128)
    contact2 = models.CharField(max_length=128)
    email = models.EmailField(null=True,blank=True)
    photo = VersatileImageField(upload_to="uploads/auditoriums/",blank=True,null=True)
    rent = models.DecimalField(default=0.00,blank=True,decimal_places=2, max_digits=15, validators=[MinValueValidator(Decimal('0.00'))])
    is_deleted = models.BooleanField(default=False)

    class Meta:
        db_table = 'auditoriums'
        verbose_name = _('auditorium')
        verbose_name_plural = _('auditorium')
        ordering = ('name',)
        
    class Admin:
        list_display = ('name',)

    def __unicode__(self):
        return self.name