from django.utils.translation import ugettext_lazy as _
from django import forms
from django.forms.widgets import TextInput,Textarea,Select,DateInput, CheckboxInput
from django.contrib.auth.models import User
from auditoriums.models import Auditorium
from dal import autocomplete


class AuditoriumForm(forms.ModelForm):

	class Meta:
		model = Auditorium
		exclude = ['creator','updator','auto_id','is_deleted']
		widgets = {
			'partner': autocomplete.ModelSelect2(url='partners:partner_autocomplete', attrs={'data-placeholder': 'Partner', 'data-minimum-input-length': 1},), 
			'name': TextInput(attrs={'class': 'required form-control','placeholder' : 'Name'}),
			'title': TextInput(attrs={'class': 'required form-control','placeholder' : 'Title'}),
			'address': TextInput(attrs={'class': 'required form-control','placeholder' : 'Address'}),
			'location': TextInput(attrs={'class': 'required form-control','placeholder' : 'Location'}),
			'city': TextInput(attrs={'class': 'required form-control','placeholder' : 'City'}),
			'district': TextInput(attrs={'class': 'required form-control','placeholder' : 'District'}),
			'state': TextInput(attrs={'class': 'required form-control','placeholder' : 'State'}),
			'zipcode': TextInput(attrs={'class': 'required form-control','placeholder' : 'ZIP'}),
			'contact1': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Contact 1'}),
			'contact2': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Contact 2'}),
			'email': TextInput(attrs={'class': 'required form-control','placeholder' : 'Email'}),
			'rent': TextInput(attrs={'class': 'required form-control number','placeholder' : 'Email'}),
			
		}

		error_messages = {
			'name' : {
				'required' : _("Product field is required."),
			},
			'Address' : {
				'required' : _("Offer percentage field is required."),
			},
			'district' : {
				'required' : _("Start date field is required."),
			},
			'state' : {
				'required' : _("End Date field is required."),
			},
			'contact1' : {
				'required' : _("End Date field is required."),
			},
			'contact2' : {
				'required' : _("End Date field is required."),
			},
			'zipcode' : {
				'required' : _("End Date field is required."),
			},
		}
